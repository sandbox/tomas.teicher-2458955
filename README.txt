-- INSTALLATION --

* Install as usual Drupal module, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --
This payment method can be configured by users with permission "Administer payment methods".
1. This payment method can be configured as other payment methods. After enabling the module, "CSOB GP WebPay" should appear in the list of payment methods,
(by default it is page /admin/commerce/config/payment-methods)
2. After clicking on "CSOB GP WebPay" in the list, 'Editing reaction rule "CSOB GP WebPay"' page appears
"CSOB GP WebPay" settings can be edited in "Actions" table


-- HOW IT WORKS

When CSOB GP WebPay payment method is enabled, it can be chosen by users as payment method during checkout process.
When customer choose CSOB GP WebPay, user is redirected offsite to third-party (CSOB GP WebPay) website, where the payment will be processed. 
After payment is processed, user is redirected from CSOB GP WebPay website back to eshop to continue (and finish) checkout process.
After return to eshop, this module automatically update status of payment transaction. 

Admistrators of eshop can check payment transaction status of particular orders on "Payment" tab on order page (admin/commerce/orders/[ORDER ID]/payment)
Information about payment is saved in Commerce Payment transaction entity. For CSOB GP WebPay, entity contains following data from external party:
field_srcode - information from SRCODE parameter
field_prcode - information from PRCODE parameter
remote_status - information from RESULTTEXT parameter
payload - information from parameters DIGEST and DIGEST1